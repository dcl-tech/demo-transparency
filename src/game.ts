////////////////////////////////////////////////
// demo-transparency
// (c) 2019 Carl Fravel
// Open Source, Licensed under the Apache 2 license
////////////////////////////////////////////////

import {spawnGltfX} from './modules/SpawnerFunctions'

const transparencyDemoShape = new GLTFShape("models/DecentralandBlenderTransparencyDemo.glb")

const transparencyDemo = spawnGltfX(transparencyDemoShape, 16, 0, 16, 0, 180, 0, 1, 1, 1)

